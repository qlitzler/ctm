package api


data class ApiCache(
    val cacheDirectory: String,
    val cacheFile: String,
    val cacheSize: Long
)