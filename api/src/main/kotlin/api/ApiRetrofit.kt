package api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url


internal interface ApiRetrofit {

    @Streaming
    @GET
    fun getText(@Url url: String): Call<ResponseBody>
}