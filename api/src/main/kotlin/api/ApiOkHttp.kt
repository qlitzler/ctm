package api

import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit


internal class ApiOkHttp(
    apiCache: ApiCache,
    debug: Boolean
) {

    private val okHttpInterceptorLogging = HttpLoggingInterceptor().also {
        it.level = if (debug) HttpLoggingInterceptor.Level.BASIC else HttpLoggingInterceptor.Level.NONE
    }

    private val cacheControl: CacheControl = CacheControl.Builder().also {
        it.maxStale(Integer.MAX_VALUE, TimeUnit.SECONDS)
    }.build()

    private val okHttpInterceptorCacheControl = Interceptor { chain ->
        val request = chain.request()
        val newRequest = request
            .newBuilder()
            .cacheControl(cacheControl)
            .build()
        chain.proceed(newRequest)
    }

    val client: OkHttpClient = OkHttpClient.Builder().also {
        it.addInterceptor(okHttpInterceptorCacheControl)
        it.addInterceptor(okHttpInterceptorLogging)
        it.cache(Cache(File(apiCache.cacheDirectory, apiCache.cacheFile), apiCache.cacheSize))
    }.build()
}