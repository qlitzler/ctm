package api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


internal class ApiClient(
    apiUrl: String,
    okHttpClient: OkHttpClient
) {

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(apiUrl)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val api: ApiRetrofit = retrofit.create(ApiRetrofit::class.java)
}