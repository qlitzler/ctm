package api

import okhttp3.ResponseBody


internal fun ResponseBody.toText(): String {
    val reader = byteStream().bufferedReader()
    val text = reader.readText()

    reader.close()
    return text
}