package api

import model.Response
import model.ResponseError
import model.ResponseNetworkError
import model.ResponseSuccess


class ApiRepository(
    apiUrl: String,
    apiCache: ApiCache,
    debug: Boolean
) {

    private val okHttp = ApiOkHttp(
        apiCache = apiCache,
        debug = debug
    )
    private val client = ApiClient(
        apiUrl = apiUrl,
        okHttpClient = okHttp.client
    )

    suspend fun getText(url: String): Response<String> {
        return try {
            val response = client.api.getText(url).execute()

            val body = response.body()
            if (response.isSuccessful && body != null) {
                ResponseSuccess(body.toText())
            } else {
                ResponseError(response.code(), response.errorBody().toString())
            }
        } catch (exception: Exception) {
            ResponseNetworkError(exception)
        }
    }
}