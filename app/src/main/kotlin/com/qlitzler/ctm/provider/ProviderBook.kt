package com.qlitzler.ctm.provider

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.qlitzler.ctm.R
import com.qlitzler.ctm.extension.ctm
import kotlinx.coroutines.experimental.Job
import model.Book


class ProviderBook(application: Application) : AndroidViewModel(application) {

    private val job = Job()

    val book = MutableLiveData<List<Book>>()

    init {
        val book1 = Book(
            id = "1",
            name = "Railway Children",
            url = ctm.getString(R.string.book_1)
        )
        book.postValue(listOf(book1))
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}