package com.qlitzler.ctm.provider

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.qlitzler.ctm.business.generatePrime
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch


class ProviderPrime(application: Application) : AndroidViewModel(application) {

    private val job = Job()

    val primeNumbers = MutableLiveData<IntArray>()

    init {
        generatePrimeNumbers()
    }

    private fun generatePrimeNumbers() {
        launch(job) {
            primeNumbers.postValue((0..50000).generatePrime())
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}