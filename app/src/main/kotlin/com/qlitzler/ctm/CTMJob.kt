package com.qlitzler.ctm

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.experimental.Job


class CTMJob : LifecycleObserver {

    val parent = Job()

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        parent.cancel()
    }
}