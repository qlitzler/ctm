package com.qlitzler.ctm.business

import model.Detail


fun String.mapWordCount(): List<Detail> {
    val regex = Regex("[\\w']+")

    return this
        .lineSequence()
        .flatMap { line -> regex.findAll(line).map { it.value } }
        .groupingBy { it }
        .eachCount()
        .map { Detail(it.key, it.value) }
}