package com.qlitzler.ctm.business

import android.support.v7.app.AppCompatActivity
import com.qlitzler.ctm.activity.ActivityDetail
import kotlinx.serialization.json.JSON
import model.Book
import org.jetbrains.anko.startActivity


enum class Field {
    Book
}

fun AppCompatActivity.startActivityDetail(book: Book) {
    startActivity<ActivityDetail>(Field.Book.name to JSON.stringify(book))
}