package com.qlitzler.ctm.business


fun IntRange.generatePrime() =
    this.map { it to it.isNotPrime() }
        .filterNot { it.second }
        .map { it.first }
        .toIntArray()

fun Int.isNotPrime() = if (this <= 1) true else (2..this / 2).any { this % it == 0 }