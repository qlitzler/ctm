package com.qlitzler.ctm.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.qlitzler.ctm.R
import com.qlitzler.ctm.adapter.AdapterBook
import com.qlitzler.ctm.connection.ConnectionBook
import com.qlitzler.ctm.extension.subscribe
import component.book.BookComponent
import component.extension.inflate
import kotlinx.android.synthetic.main.activity.*


class ActivityBook : AppCompatActivity() {

    private val adapterBook = AdapterBook(this)
    private val book = BookComponent(adapterBook, adapterBook.ItemDecoration())
    private val connectionBook = ConnectionBook(this, adapterBook)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)

        connectionBook.subscribe(this)

        book
            .inflate(container, true)
            .subscribe(this)

        setSupportActionBar(book.toolbar)
    }
}