package com.qlitzler.ctm.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.qlitzler.ctm.CTMJob
import com.qlitzler.ctm.R
import com.qlitzler.ctm.adapter.AdapterDetail
import com.qlitzler.ctm.business.Field
import com.qlitzler.ctm.connection.ConnectionAdapterDetail
import com.qlitzler.ctm.connection.ConnectionDetail
import com.qlitzler.ctm.extension.subscribe
import component.detail.DetailComponent
import component.extension.inflate
import kotlinx.android.synthetic.main.activity.*
import kotlinx.serialization.json.JSON
import model.Book


class ActivityDetail : AppCompatActivity() {

    private val job = CTMJob()
    private val connectionAdapter = ConnectionAdapterDetail(this, job.parent)
    private val adapter = AdapterDetail(this, connectionAdapter)
    private val detail = DetailComponent(adapter, adapter.ItemDecoration())
    private val connection = ConnectionDetail(this, adapter, job.parent)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)

        val book = JSON.parse<Book>(intent.getStringExtra(Field.Book.name))

        detail
            .inflate(container, true)
            .subscribe(this)

        job.subscribe(this)
        connection.subscribe(this)
        connectionAdapter.subscribe(this)

        setSupportActionBar(detail.toolBar)
        connection.getText(book.url)

        supportActionBar?.let {
            it.title = book.name
            it.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}