package com.qlitzler.ctm.adapter

import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.qlitzler.ctm.connection.ConnectionAdapterDetail
import component.detail.DetailItemComponent
import model.Detail
import org.jetbrains.anko.dip


class AdapterDetail(
    private val activity: AppCompatActivity,
    private val connection: ConnectionAdapterDetail
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val margin by lazy { activity.dip(8) }

    var items = listOf<Detail>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DetailItemComponent(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when (holder) {
            is DetailItemComponent -> {
                holder.bind(
                    word = item.word,
                    count = item.count
                )
                connection.isPrime(holder, item.count)
            }
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        when (holder) {
            is DetailItemComponent -> connection.cancel(holder)
        }
    }

    override fun getItemCount() = items.size

    inner class ItemDecoration : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view)
            val top = if (position == 0) margin * 2 else margin
            val bottom = if (position == items.lastIndex) margin * 2 else margin

            outRect.set(margin, top, margin, bottom)
        }
    }
}