package com.qlitzler.ctm.adapter

import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.qlitzler.ctm.business.startActivityDetail
import component.book.BookItemComponent
import model.Book
import org.jetbrains.anko.dip


class AdapterBook(
    private val activity: AppCompatActivity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val margin by lazy { activity.dip(8) }

    var items = listOf<Book>()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val book = items[position]
        when (holder) {
            is BookItemComponent -> {
                holder.bind(
                    name = book.name,
                    url = book.url,
                    onClickListener = View.OnClickListener { activity.startActivityDetail(book) }
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BookItemComponent(parent)
    }

    override fun getItemCount(): Int = items.size

    inner class ItemDecoration : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view)
            val top = if (position == 0) margin * 2 else margin
            val bottom = if (position == items.lastIndex) margin * 2 else margin

            outRect.set(margin, top, margin, bottom)
        }
    }
}