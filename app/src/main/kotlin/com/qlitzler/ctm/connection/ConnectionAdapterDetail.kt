package com.qlitzler.ctm.connection

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import com.qlitzler.ctm.extension.observeNotNull
import com.qlitzler.ctm.extension.providerPrime
import component.detail.DetailItemComponent
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext


class ConnectionAdapterDetail(
    override val activity: AppCompatActivity,
    private val job: Job
) : Connection {

    private val mapObservers = mutableMapOf<DetailItemComponent, Observer<IntArray>>()
    private val mapJobs = mutableMapOf<DetailItemComponent, Job>()

    fun isPrime(holder: DetailItemComponent, number: Int) {
        removeObserver(holder)
        removeJob(holder)
        providerPrime.primeNumbers.observeNotNull(activity) { primes ->
            mapJobs.put(holder, launch(job) {
                val isPrime = primes.binarySearch(number) > 0

                withContext(UI) { holder.setIsPrime(isPrime) }
            })
        }
    }

    fun cancel(holder: DetailItemComponent) {
        removeObserver(holder)
        removeJob(holder)
    }

    private fun removeObserver(holder: DetailItemComponent) {
        val previous = mapObservers.remove(holder)
        if (previous != null) {
            providerPrime.primeNumbers.removeObserver(previous)
        }
    }

    private fun removeJob(holder: DetailItemComponent) {
        mapJobs.remove(holder)?.cancel()
    }
}