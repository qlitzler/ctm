package com.qlitzler.ctm.connection

import android.arch.lifecycle.LifecycleObserver
import android.support.v7.app.AppCompatActivity


interface Connection : LifecycleObserver {

    val activity: AppCompatActivity
}