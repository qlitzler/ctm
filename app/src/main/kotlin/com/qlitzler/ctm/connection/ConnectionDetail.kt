package com.qlitzler.ctm.connection

import android.support.v7.app.AppCompatActivity
import com.qlitzler.ctm.adapter.AdapterDetail
import com.qlitzler.ctm.business.mapWordCount
import com.qlitzler.ctm.extension.ctm
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import model.ResponseSuccess


class ConnectionDetail(
    override val activity: AppCompatActivity,
    private val adapter: AdapterDetail,
    private val job: Job
) : Connection {

    fun getText(url: String) {
        launch(job) {
            val response = activity.ctm.api.getText(url)

            when (response) {
                is ResponseSuccess -> {
                    adapter.items = response.data.mapWordCount().sortedByDescending { it.count }
                    withContext(UI) { adapter.notifyDataSetChanged() }
                }
            }
        }
    }
}