package com.qlitzler.ctm.connection

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.app.AppCompatActivity
import com.qlitzler.ctm.adapter.AdapterBook
import com.qlitzler.ctm.extension.observeNotNull
import com.qlitzler.ctm.extension.providerBook


class ConnectionBook(
    override val activity: AppCompatActivity,
    private val adapter: AdapterBook
) : Connection {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun book() {
        providerBook.book.observeNotNull(activity) { books ->
            adapter.items = books
            adapter.notifyDataSetChanged()
        }
    }
}