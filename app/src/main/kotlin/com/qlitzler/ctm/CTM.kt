package com.qlitzler.ctm

import android.app.Application
import api.ApiCache
import api.ApiRepository


class CTM : Application() {

    lateinit var api: ApiRepository

    override fun onCreate() {
        super.onCreate()

        api = ApiRepository(
            apiUrl = getString(R.string.api_url),
            apiCache = ApiCache(
                cacheDirectory = cacheDir.toString(),
                cacheFile = "ctm",
                cacheSize = 500 * 1024 * 1024
            ),
            debug = BuildConfig.DEBUG
        )
    }
}