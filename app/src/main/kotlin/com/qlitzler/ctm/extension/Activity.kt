package com.qlitzler.ctm.extension

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import com.qlitzler.ctm.CTM


inline fun <reified T : AndroidViewModel> AppCompatActivity.provider(): T {
    return ViewModelProviders.of(this).get(T::class.java)
}


inline fun <reified T : LifecycleObserver> T.subscribe(activity: AppCompatActivity): T {
    activity.lifecycle.addObserver(this)
    return this
}

inline val AppCompatActivity.ctm: CTM get() = applicationContext as CTM