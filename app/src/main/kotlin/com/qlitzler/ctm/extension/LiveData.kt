package com.qlitzler.ctm.extension

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer


fun <T> LiveData<T>.observe(owner: LifecycleOwner, completion: (T?) -> Unit) {
    observe(owner, Observer(completion))
}

fun <T> LiveData<T>.observeNotNull(owner: LifecycleOwner, completion: (T) -> Unit) {
    observe(owner, Observer { data -> if (data != null) completion(data) })
}