package com.qlitzler.ctm.extension

import com.qlitzler.ctm.connection.Connection
import com.qlitzler.ctm.provider.ProviderBook
import com.qlitzler.ctm.provider.ProviderPrime


inline val Connection.providerPrime get() = activity.provider<ProviderPrime>()
inline val Connection.providerBook get() = activity.provider<ProviderBook>()