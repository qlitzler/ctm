package com.qlitzler.ctm.extension

import android.arch.lifecycle.AndroidViewModel
import com.qlitzler.ctm.CTM


inline val AndroidViewModel.ctm get() = getApplication<CTM>()