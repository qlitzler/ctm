package model


data class Detail(
    val word: String,
    val count: Int
)