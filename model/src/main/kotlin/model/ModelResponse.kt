package model


sealed class Response<T>

class ResponseSuccess<T>(val data: T) : Response<T>()

class ResponseError<T>(val code: Int, val body: String? = null) : Response<T>()

class ResponseNetworkError<T>(val exception: Exception) : Response<T>()