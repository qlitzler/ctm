package model

import kotlinx.serialization.Serializable

@Serializable
data class Book(
    val id: String,
    val name: String,
    val url: String
)