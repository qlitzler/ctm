package component.extension

import android.view.ViewGroup
import component.Component
import org.jetbrains.anko.layoutInflater


fun <T : Component> T.inflate(parent: ViewGroup, attachToRoot: Boolean = false): T {
    view = parent.context.layoutInflater.inflate(layoutId, parent, attachToRoot)
    return this
}