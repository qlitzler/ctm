package component.book

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import component.Component
import component.R
import kotlinx.android.synthetic.main.book.view.*


class BookComponent(
    private val adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>,
    private val itemDecoration: RecyclerView.ItemDecoration
) : Component {

    override lateinit var view: View

    override val layoutId: Int = R.layout.book

    val toolbar: Toolbar get() = view.bookToolbar

    private val layoutManager by lazy { LinearLayoutManager(view.context) }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        view.bookList.let {
            it.layoutManager = layoutManager
            it.adapter = adapter
            it.addItemDecoration(itemDecoration)
        }
    }
}