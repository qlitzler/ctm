package component.book


data class BookItem(
    val id: String,
    val url: String,
    val name: String
)