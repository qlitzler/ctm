package component.book

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import component.Component
import component.R
import component.extension.inflate
import kotlinx.android.synthetic.main.book_item.view.*


class BookItemComponent(
    parent: ViewGroup,
    override val layoutId: Int = R.layout.book_item,
    override var view: View = parent.inflate(layoutId)
) : RecyclerView.ViewHolder(view), Component {

    fun bind(name: String, url: String, onClickListener: View.OnClickListener) {
        view.let {
            it.bookItemName.text = name
            it.bookItemUrl.text = url
            it.setOnClickListener(onClickListener)
        }
    }
}