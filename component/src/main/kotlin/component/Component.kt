package component

import android.arch.lifecycle.LifecycleObserver
import android.view.View


interface Component: LifecycleObserver {

    var view: View
    val layoutId: Int
}