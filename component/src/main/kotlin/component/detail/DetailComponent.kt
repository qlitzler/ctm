package component.detail

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import component.Component
import component.R
import kotlinx.android.synthetic.main.detail.view.*


class DetailComponent(
    private val adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>,
    private val itemDecoration: RecyclerView.ItemDecoration
) : Component {

    override val layoutId: Int = R.layout.detail

    override lateinit var view: View

    val toolBar: Toolbar get() = view.detailToolbar

    private val layoutManager by lazy { LinearLayoutManager(view.context) }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        view.detailList.let {
            it.layoutManager = layoutManager
            it.adapter = adapter
            it.addItemDecoration(itemDecoration)
        }
    }
}