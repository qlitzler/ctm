package component.detail

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import component.Component
import component.R
import component.extension.inflate
import kotlinx.android.synthetic.main.detail_item.view.*
import org.jetbrains.anko.imageResource


class DetailItemComponent(
    parent: ViewGroup,
    override val layoutId: Int = R.layout.detail_item,
    override var view: View = parent.inflate(layoutId)
) : RecyclerView.ViewHolder(view), Component {

    fun bind(word: String, count: Int) {
        view.let {
            it.detailItemName.text = word
            it.detailItemCount.text = view.context.getString(R.string.word_occurrences, count)
        }
    }

    fun setIsPrime(isPrime: Boolean) {
        view.detailItemIcon.imageResource = if (isPrime) R.drawable.ic_check else R.drawable.ic_close
    }
}